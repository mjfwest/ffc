# Copyright (C) 2013 Marie E. Rognes
#
# This file is part of FFC.
#
# FFC is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FFC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with FFC. If not, see <http://www.gnu.org/licenses/>.
#
# Simple test form for finite elements defined over (multiple) domains
#
# Compile this form with FFC: ffc MultiDomain.ufl

D = Domain(triangle, name='Universe')

left = Region(D, (0,), name='left')
right = Region(D, (1,), name='right')

V1 = FiniteElement("CG", left, 1)
V2 = FiniteElement("CG", right, 2)

W = V1*V2

(u1, u2) = TrialFunctions(W)
(v1, v2) = TestFunctions(W)

a = u1*v1*dx(0) + u2*v2*dx(1)
